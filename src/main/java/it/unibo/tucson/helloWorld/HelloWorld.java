package it.unibo.tucson.helloWorld;

import alice.tuple.logic.LogicTuple;
import alice.tuple.logic.exceptions.InvalidLogicTupleException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import alice.tuplecentre.tucson.api.TucsonAgentId;
import alice.tuplecentre.tucson.api.TucsonMetaACC;
import alice.tuplecentre.tucson.api.TucsonOperation;
import alice.tuplecentre.tucson.api.TucsonTupleCentreId;
import alice.tuplecentre.tucson.api.acc.EnhancedSyncACC;
import alice.tuplecentre.tucson.api.acc.NegotiationACC;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tuplecentre.tucson.api.exceptions.UnreachableNodeException;

/**
 * Plain Java class exploiting TuCSoN library.
 *
 * @author s.mariani@unibo.it
 */
public class HelloWorld {

    /**
     * @param args the name of the TuCSoN coordinable (optional).
     */
    public static void main(String[] args) {

        /*
         * 1) Build a TuCSoN Agent identifier to contact the TuCSoN system.
         */
        TucsonAgentId aid = null;
        try {
            if (args.length == 0) {
                aid = TucsonAgentId.of("helloWorldMain");
            } else {
                aid = TucsonAgentId.of(args[0]);
            }
            /*
             * 2) Get a TuCSoN ACC to enable interaction with the TuCSoN system.
             */
            NegotiationACC negAcc = TucsonMetaACC.getNegotiationContext(aid);
            EnhancedSyncACC acc = negAcc.playDefaultRole();
            /*
             * 3) Define the tuplecentre target of your coordination operations.
             */
            TucsonTupleCentreId tid = TucsonTupleCentreId.of("default", "localhost", "20504");
            /*
             * 4) Build the tuple using the communication language.
             */
            LogicTuple tuple = LogicTuple.parse("hello(world)");
            /*
             * 5) Perform the coordination operation using the preferred
             * coordination primitive.
             */
            TucsonOperation op = acc.out(tid, tuple, null);
            /*
             * 6) Check requested operation success.
             */
            LogicTuple res = null;
            if (op.isResultSuccess()) {
                System.out.println("[" + aid.getAgentId() + "]: Operation succeeded.");
                /*
                 * 7) Get requested operation result.
                 */
                res = op.getLogicTupleResult();
                System.out.println("[" + aid.getAgentId() + "]: Operation result is " + res);
            } else {
                System.out.println("[" + aid.getAgentId() + "]: Operation failed.");
            }
            /*
             * Another success test to be sure.
             */
            LogicTuple template = LogicTuple.parse("hello(World)");
            op = acc.rdp(tid, template, null);
            if (op.isResultSuccess()) {
                res = op.getLogicTupleResult();
                System.out.println("[" + aid.getAgentId() + "]: Operation result is " + res);
            } else {
                System.out.println("[" + aid.getAgentId() + "]: Operation failed.");
            }
            /*
             * Automatically done by the TuCSoN Node, hence unnecessary.
             */
            acc.exit();
        } catch (TucsonInvalidAgentIdException e) {
            /*
             * The chosen TuCSoN Agent ID is not admissible.
             */
            e.printStackTrace();
        } catch (TucsonInvalidTupleCentreIdException e) {
            /*
             * The chosen target tuple centre is not admissible.
             */
            e.printStackTrace();
        } catch (TucsonOperationNotPossibleException e) {
            /*
             * The requested TuCSoN operation cannot be performed.
             */
            e.printStackTrace();
        } catch (UnreachableNodeException e) {
            /*
             * The chosen target tuple centre is not reachable.
             */
            e.printStackTrace();
        } catch (OperationTimeOutException e) {
            /*
             * Operation timeout expired.
             */
            e.printStackTrace();
        } catch (InvalidLogicTupleException e) {
            /*
             * The logic tuple to be parsed is invalid
             */
            e.printStackTrace();
        }

    }

}
